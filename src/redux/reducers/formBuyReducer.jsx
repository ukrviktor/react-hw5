import { createSlice } from '@reduxjs/toolkit';
// import { setProductsInBasket } from 'src/redux/reducers/productsReducer';

// default values for formBuy
const initialState = {
  status: false,
  name: '',
  lastName: '',
  age: 0,
  address: '',
  phone: '',
  products: [],
};

const formBuySlice = createSlice({
  name: 'formBuy',
  initialState,
  reducers: {
    actionBuy: (state, action) => {
      const { name, lastName, age, address, phone, products } = action.payload;

      // report sell
      console.log('name:', name);
      console.log('lastName:', lastName);
      console.log('age:', age);
      console.log('address:', address);
      console.log('phone:', phone);
      console.log('----- купил -----');
      const price = products.reduce((totalPrice, item, index) => {
        console.log(`${index + 1}. ${item.name} - ${item.count} шт.`, item);
        return totalPrice + item.price * item.count;
      }, 0);
      console.log('----- итого -----');
      console.log(`${price} грн.`);

      // clear localStorage
      localStorage.setItem('basket', JSON.stringify([]));

      return {
        status: true,
        name,
        lastName,
        age,
        address,
        phone,
        products,
      };
    },
    clearStatus: (state, actions) => {
      state.status = false;
    },
  },
});

export const { actionBuy } = formBuySlice.actions;
export default formBuySlice.reducer;
