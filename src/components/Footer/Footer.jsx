import React from 'react';
import styles from './Footer.module.scss';

export const Footer = () => {
  return <div className={styles.Footer__row}>&copy; Hannocheko 2023</div>;
};
