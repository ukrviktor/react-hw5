import * as yup from 'yup';

export const validationSchema = yup.object().shape({
  name: yup
    .string()
    .matches(
      /^[a-zA-Zа-яА-Я ]+$/,
      'Введіть будь ласка тільки англійськи символи'
    )
    .min(3, 'Імя має бути не менше 3 символів')
    .required('Обовязкове поле'),
  lastName: yup
    .string()
    .matches(
      /^[a-zA-Zа-яА-Я ]+$/,
      'Введіть будь ласка тільки англійськи символи'
    )
    .min(3, 'Прізвище має бути не менше 3 символів')
    .required('Обовязкове поле'),
  age: yup
    .number()
    .typeError('Має бути число')
    .min(13, 'Число має бути більше 12')
    .required('Обовязкове поле'),
  address: yup
    .string()
    .min(3, 'Адреса має бути не менше 3 символів')
    .required('Обовязкове поле'),
  phone: yup
    .string()
    .matches(
      /^\(\d{3}\)\d{3}-\d{2}-\d{2}$/,
      'Номер телефона повинен бути в форматі (###)###-##-##'
    )
    .required('Обовязкове поле'),
});
